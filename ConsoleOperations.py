import sys
import os
from subprocess import call


class ConsoleOperations(object):

    def clear_console(self):
        if sys.platform == "win32":
            os.system('cls')
        else:
            call('clear')
