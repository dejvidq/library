from DataBaseOperations import DataBaseOperations
from ConsoleOperations import ConsoleOperations
import sys
from datetime import date
from PIL import Image
import os
from Variables import *

#TODO: add possibility (or download automatically) cover of books in library and display them if user wants to
#TODO: change behaviour of methods - add 'return' to their body where possible
#TODO: write unit tests


class Library(object):

    def __init__(self):
        self.db = DataBaseOperations()
        self.cmd = ConsoleOperations()

    def run(self):
        self.cmd.clear_console()
        print("*** Library ***")
        self.db.db_connect()
        message = """Choose what would you like to do:
            1. Get book by title
            2. Get all books in library
            3. Add new book
            4. Clear your library
            5. Edit book in library
            6. Display book cover
            7. Delete single book

            0. Exit\n"""
        while True:
            print(message)
            choice = ''
            while not isinstance(choice, int):
                try:
                    choice = int(input("Choose operation: "))
                    break
                except ValueError:
                    print('Remember, only numbers')

            self._switch(choice)
            input("\nPress enter to continue...")
            self.cmd.clear_console()

    def _get_all_books(self):
        self.cmd.clear_console()
        book_list = self.db.get_all_books()
        print(book_list)

    def _get_book_by_title(self):
        self.cmd.clear_console()
        title = input("Input title of book about which you want to get details: ")
        book_details = self.db.get_book_details(title)
        print(book_details)

    def _add_book(self, title=''):
        """
        TODO: extract smaller private functions from inside
        """
        self.cmd.clear_console()
        try:
            while True:
                while not title:
                    title = input("Enter title of book (it can't be empty): ")
                author = input("Enter an author (or authors) of this book (can be ommited): ")
                while True:
                    year = input("Enter year of publishing (can be ommited): ")
                    try:
                        year = int(year)
                    except ValueError:
                        pass
                    if year == '':
                        year = -1
                        break
                    elif isinstance(year, int) and year in range(0, date.today().year+1):
                        break
                genre = input("Enter genre of the book (can be ommited): ")
                while True:
                    cover = input('Enter absolute path to book cover (file must exist but can be omitted): ')
                    if cover == "" or self.__check_cover_path_valid(cover):
                        break
                self.db.add_book(title, author, year, genre, cover)
                print("Book added!")
                break
        except KeyboardInterrupt:
            pass

    def _finish(self):
        print("Are you sure (y/n)?", end=' ')
        response = input()
        if response.lower() == 'yes' or response.lower() == 'y':
            self.db.db_disconnect()
            print("Bye!")
            sys.exit()
        else:
            pass

    def _clear_library(self):
        self.cmd.clear_console()
        if input("Are you sure you want to delete all books (y/n)? ").lower() == 'y':
            self.db.clear_all_table()
            print("Library is empty!")
        else:
            pass

    def _switch(self, case):
        if case == 0:
            self._finish()
        elif case == 1:
            self._get_book_by_title()
        elif case == 2:
            self._get_all_books()
        elif case == 3:
            self._add_book()
        elif case == 4:
            self._clear_library()
        elif case == 5:
            self._edit_book()
        elif case == 6:
            self._display_book_cover()
        elif case == 7:
            self._delete_single_book()
        else:
            print("There is no such operation as you typed")

    def _edit_book(self):
        title = ''
        choice = -1
        self.cmd.clear_console()
        while not title:
            title = input("Enter title of book to edit: ")
        book = self.db.get_book_details(title)
        if len(book) == 0:
            print("There are no books with title '%s'" % title)
        print("\nBook with title '%s' has fields as presented below:" % title)
        print(book)
        while True:
            message = """\nWhich field you'd like to change (only one at a time is possible)? 
            1. Title
            2. Author
            3. Release date
            4. Genre
            5. Cover
            
            0. Finish"""
            print(message)
            try:
                choice = int(input("Choose edit field: "))
            except ValueError:
                print("Only numbers, remember.")
            if choice == 1:
                self.__edit_title(title)
            elif choice == 2:
                self.__edit_author(title)
            elif choice == 3:
                self.__edit_release_date(title)
            elif choice == 4:
                self.__edit_genre(title)
            elif choice == 5:
                self.__edit_cover(title)
            elif choice == 0:
                break
            else:
                print("There is no such operation")

    def _display_book_cover(self):
        title = ''
        column_name = 'cover'
        cover_dir_path = os.path.join(os.getcwd(), covers_dir)
        while not title:
            title = input("Enter title of book which you want to display cover: ")
        cover_name = self.db.get_column(column_name, title)
        cover_path = os.path.join(cover_dir_path, cover_name)
        if cover_path != -1 and cover_path != '' and self.__check_cover_path_valid(cover_path):
            img = Image.open(cover_path)
            img.show()
        else:
            print("Something is not ok or there's no cover in db")

    def _delete_single_book(self):
        title = ''
        while not title:
            title = input("Enter title of book you want to delete: ")
        number_of_books_with_same_title = self.db.count_in_library_by_title(title)
        if number_of_books_with_same_title == 0:
            print("There is no book with title %s in library!" % title)
        elif number_of_books_with_same_title == 1:
            self.db.delete_book('title', title)
        elif number_of_books_with_same_title >= 2:
            book_id = -1
            books = self.db.get_book_details(title)
            possible_ids = self.db.get_book_id(title)
            print(books)
            while True:
                try:
                    book_id = int(input("Enter ID of book you want to delete: "))
                except Exception as e:
                    print(e)
                if book_id in possible_ids:
                    break
                else:
                    print("Book with id=%s doesn't have title '%s'" % (book_id, title))
            self.db.delete_book('id', book_id)
        print("Book was deleted successfully")

    def __editing_complete(self, title):
        self.cmd.clear_console()
        print("Editing completed!\nNow this book has fields as follows:")
        edited_book = self.db.get_book_details(title)
        return edited_book

    def __edit_title(self, title):
        new_title = ''
        while not new_title:
            new_title = input("Enter new title: ")
        self.db.edit_book(title, 'title', new_title)
        print(self.__editing_complete(new_title))

    def __edit_author(self, title):
        new_author = input("Enter new author: ")
        self.db.edit_book(title, "author", new_author)
        print(self.__editing_complete(title))

    def __edit_release_date(self, title):
        new_release_date = -1
        while new_release_date not in range(0, date.today().year + 1):
            try:
                new_release_date = int(input("Enter new release date: "))
                if new_release_date not in range(0, date.today().year + 1):
                    print("Release dates from 0 to 2017 are allowed")
            except ValueError:
                print("Only numbers are allowed")
        self.db.edit_book(title, 'release_date', new_release_date)
        print(self.__editing_complete(title))

    def __edit_genre(self, title):
        new_genre = input("Enter new genre: ")
        self.db.edit_book(title, "genre", new_genre)
        print(self.__editing_complete(title))

    def __edit_cover(self, title):
        try:
            while True:
                print(os.getcwd())
                new_cover = input("Enter new path to cover (path must exist and image must have proper file format): ")
                if new_cover == '' or self.__check_cover_path_valid(new_cover):
                    self.db.edit_book(title, "cover", new_cover)
                    print(self.__editing_complete(title))
                    break
        except KeyboardInterrupt:
            pass

    def __check_cover_path_valid(self, cover_name):
        cover_path = os.path.join(os.getcwd(), covers_dir)
        if cover_name == '':
            return True
        cover_name = os.path.join(cover_path, cover_name)
        if os.path.isfile(cover_name):
            file_format = cover_name.split('.')[-1]
            if file_format in img_formats:
                return True
        return False

if __name__ == "__main__":
    library = Library()
    library.run()
