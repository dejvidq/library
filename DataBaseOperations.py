import psycopg2
import sys
import os
from tabulate import tabulate
from Variables import *

#TODO change behaviour of methods - add 'return' to their body where possible
class DataBaseOperations(object):

    def __init__(self):
        self.db = None
        self.cursor = None
        self.db_name = os.environ['DB_NAME']
        self.db_user = os.environ['DB_USER']
        self.db_password = os.environ['DB_PASSWORD']
        self.db_table_name = os.environ['DB_TABLE']

    def db_connect(self, database=None, user=None, password=None):
        if database is None:
            database = self.db_name
        if user is None:
            user = self.db_user
        if password is None:
            password = self.db_password
        try:
            self.db = psycopg2.connect(dbname=database, user=user, password=password)
            self.cursor = self.db.cursor()
        except psycopg2.DatabaseError as e:
            print('Error: %s' % e)
            sys.exit(1)

    def db_disconnect(self):
        if self.db:
            self.db.close()

    def db_execute(self, query):
        self.cursor.execute(query)

    def get_all_books(self):
        query = "select * from %s" % self.db_table_name
        self.db_execute(query)
        books = self.cursor.fetchall()
        if len(books) == 0:
            return "There are no books in library"
        else:
            books = [list(book) for book in books]
        return tabulate(books, headers)

    def add_book(self, title, author="", release_date="", genre='', cover=''):
        query = "insert into %s (title, author, release_date, genre, cover) values ('%s', '%s', %s, '%s', '%s')" % \
                (self.db_table_name, title, author, release_date, genre, cover)
        self.db_execute(query)
        self.db.commit()

    def clear_all_table(self):
        query = "TRUNCATE TABLE %s; ALTER SEQUENCE %s_id_seq RESTART WITH 1" % (self.db_table_name, self.db_table_name)
        self.db_execute(query)
        self.db.commit()

    def get_book_details(self, title):
        query = "SELECT * FROM %s WHERE title='%s'" % (self.db_table_name, title)
        self.db_execute(query)
        result = self.cursor.fetchall()
        return tabulate(result, headers)

    def edit_book(self, title, column_name, new_value):
        query = "UPDATE %s set %s='%s' WHERE title='%s'" % (self.db_table_name, column_name, new_value, title)
        self.cursor.execute(query)
        self.db.commit()

    def get_column(self, column_name, title):
        query = "SELECT %s FROM %s WHERE title='%s'" % (column_name, self.db_table_name, title)
        self.cursor.execute(query)
        try:
            result = self.cursor.fetchall()[0][0]
        except IndexError as e:
            print("ERROR: %s" % e)
            print("It doesn't exist")
            result = -1
        return result

    def count_in_library_by_title(self, title):
        query = "SELECT count(*) FROM %s where title='%s'" % (self.db_table_name, title)
        self.cursor.execute(query)
        try:
            result = self.cursor.fetchall()[0][0]
        except IndexError as e:
            print("ERROR: %s" % e)
            result = -1
        return result

    def delete_book(self, column_name, value):
        query = "DELETE FROM %s WHERE %s='%s'" % (self.db_table_name, column_name, value)
        self.cursor.execute(query)
        self.db.commit()

    def get_book_id(self, title):
        query = "SELECT id FROM %s where title='%s'" % (self.db_table_name, title)
        self.cursor.execute(query)
        result = self.cursor.fetchall()
        if len(result) == 1:
            result = result[0][0]
        elif len(result) >= 2:
            result = [res[0] for res in result]
        return result
