This is a simple project to manage home library (books, movies, games).

For now it's console project, but I hope I'll made also GUI for this to be more "user friendly".

For now with this project you can:

1. Add book to library

2. Get book from library by title

3. Get all books from library

4. Delete all library

5. Edit book in library

TODO in future:

1. Search by author

and much more, this list will be updated over time.